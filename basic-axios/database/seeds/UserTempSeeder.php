<?php

use Illuminate\Database\Seeder;
use App\UserTemp;

class UserTempSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data1 = UserTemp::create([
        	'name' => 'Muhammad Iqbal Mubarok',
        ]);

        $data1->save();

        $data2 = UserTemp::create([
        	'name' => 'Ruby Purwanti',
        ]);

        $data2->save();

        $data3 = UserTemp::create([
            'name' => 'Faqih Muhammad',
        ]);

        $data3->save();
    }
}
