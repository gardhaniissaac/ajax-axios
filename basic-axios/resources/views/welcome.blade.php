<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
​
        <title>Ajax Request With Axios</title>
​
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div id="app">
​            <input type="text" v-model="new_user">
            <button v-on:click="add" v-show="!updateSubmit">Add</button>
            <button v-on:click="update" v-show="updateSubmit">Update</button>
            <ul>
                <li v-for = "data in users">
                    @{{ data.name }}
                    <button style="margin-left: 10px" v-on:click="edit(data.name,data.id)">Edit</button>
                    ||
                    <button v-on:click="hapus(data.id)">Delete</button>
                    <br><br>
                </li>
            </ul>
       </div>
​       
        <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
        <script src="/js/vue-script.js"></script>
        <script type="text/javascript">
            var app = new Vue({
                el: '#app',
                data: {
                    message: 'Hello Vue!',
                    new_user: '',
                    users: [],
                    updateSubmit: false,
                    dummy_index: 0
                },
                methods: {
                    add: function(){
                        user_temp = this.new_user.split("\n").join("").split(" ").join("")
                        if(user_temp.length == 0){
                            this.new_user = ''
                            return alert('Nama user kosong. Harap diisi!')
                        }
                        axios.post('/api/usertemp/create', {name: this.new_user}).then(response =>{
                            this.users.push(response.data)
                            this.new_user = ''
                        });
                    },
                    update: function(){
                        user_temp = this.new_user.split("\n").join("").split(" ").join("")
                        if(user_temp.length == 0){
                            this.new_user = ''
                            return alert('Nama user kosong. Harap diisi!')
                        }
                        axios.put('/api/usertemp/update/'+this.dummy_index, {name: this.new_user}).then(response => {
                            this.users.find(x => x.id === this.dummy_index).name = this.new_user
                            this.new_user = ''
                            this.dummy_index = 0
                            this.updateSubmit = false
                        }).catch(error => {console.log(error)});
                    },
                    edit: function(name,id){
                        this.new_user = name
                        this.updateSubmit = true,
                        this.dummy_index = id
                    },
                    hapus: function(id){
                        let con = confirm('Anda yakin untuk menghapus data ini?')

                        if(con){
                            axios.delete('/api/usertemp/delete/'+id).then(response => {
                                console.log("Data berhasil dihapus")
                            }).catch(error => {console.log(error)});
                            this.users.splice(this.users.findIndex(x => x.id === id),1)
                        }
                    }
                },
                mounted() {
                    axios.get('/api/usertemp').then(response => this.users = response.data);
                }
            })
        </script>
    </body>
</html>