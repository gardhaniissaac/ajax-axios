<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserTemp;

class UserTempController extends Controller
{
    public function index()
    {
        $data = UserTemp::all();
        return $data;
    }

    public function store(Request $request)
    {
        $data = UserTemp::create([
            'name' => $request['name'],
        ]);

        return $data;
    }

    public function update(Request $request, $id)
    {
        $data = UserTemp::find($id);

        $data->update([
            'name' => $request['name'],
        ]);

        return $data;
    }

    public function destroy($id)
    {
        $data = UserTemp::destroy($id);

        return "Data berhasil dihapus";
    }
}
